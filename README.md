**Dependencies:** requests (pip install requests)

**Authentication:** You need to authenticate this script with the username and an 
app password from your Bitbucket account.

1. Go to https://bitbucket.org/account/settings/app-passwords/new

2. Create a new app password and give it Read & Write access for Pull Requests

3. Provide the credentials by setting BITBUCKET_USERNAME and BITBUCKET_APP_PASSWORD in
    your environment variables, or updating the variables directly in the script, or by
    passing `--user BITBUCKET_USERNAME:BITBUCKET_APP_PASSWORD` as CLI args.

**Usage:**

    multipr.py pullrequest branch [branch ...]

        pullrequest
            Either URL for Bitbucket pull request:
                https://bitbucket.org/{user}/{repo}/pull-requests/{id}
            or an identifier in this form:
                {user}/{repo}/{id}
        branch
            Branches in the destination repo that the PR will be duplicated to.
            The duped PRs will have the same description, tagged reviewers, etc.

**Notes:**

* Run `multipr.py -h` to see all options.
* If the original PR is updated, you can re-run the script with the same args to replicate
  the updates to respective branches.
* This script's effects are idempotent.
