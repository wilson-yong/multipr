#!/usr/bin/python3

"""
Duplicates a pull request to branches in the same destination repository

Usage:
    multipr.py pullrequest branch [branch ...]
"""

from argparse import ArgumentParser
from collections import namedtuple
import json
from os import environ
import re
import sys
from urllib.parse import urlparse

import requests
from requests.auth import HTTPBasicAuth

from jsonpath import JsonPathIterator


# Used for Basic Auth in Bitbucket API call
BITBUCKET_USERNAME: str = environ.get('BITBUCKET_USERNAME', default=None)
BITBUCKET_APP_PASSWORD: str = environ.get('BITBUCKET_APP_PASSWORD', default=None)


PullRequest = namedtuple('PullRequest', 'user repo id')

GET_PR_URL = 'https://api.bitbucket.org/2.0/repositories/{user}/{repo}/pullrequests/{pr}'
POST_PR_URL = 'https://api.bitbucket.org/2.0/repositories/{user}/{repo}/pullrequests'

class UrlFormatError(ValueError):
    pass


def enable_debug_logging():
    import http.client as http_client
    import logging

    logging.basicConfig()
    logging.getLogger().setLevel(logging.DEBUG)

    req_logger = logging.getLogger('requests.packages.urllib3')
    req_logger.setLevel(logging.DEBUG)
    req_logger.propagate = True

    http_client.HTTPConnection.debuglevel = 1


def log_object(obj, description):
    name = '-'.join(description.lower().split())
    filename = f'./{name}.json'
    if not isinstance(obj, str):
        obj = json.dumps(obj, indent=2)
    with open(filename, 'w', encoding='utf8') as f:
        f.write(obj)
    print(f'Updated {description} ({filename})')


def confirm(pr, data, branches, yes=False):
    d = JsonPathIterator(data)
    src, dest = d.source, d.destination

    print(f'Duplicating https://bitbucket.org/{pr.user}/{pr.repo}/pull-requests/{pr.id}\n'
          f'    {src.repository.full_name._}:{src.branch.name._} -> '
          f'{dest.repository.full_name._}:{dest.branch.name._}')

    if not yes:
        try:
            print(f'into these branches:')
            for branch in branches:
                print(f'    {branch}')

            choice = input('Is this correct? [Y/n] ').lower()
            if choice in ['n', 'no']:
                print('Aborted.')
                sys.exit(0)

        except KeyboardInterrupt:
            print()
            sys.exit(0)


def multipr(pr_url, branches, dry_run=False, skip_check=False, verbosity=0):
    # GET pull request data from Bitbucket API
    pr = parse_url(pr_url)
    data = query_pr(*pr, verbosity=verbosity)

    confirm(pr, data, branches, skip_check or dry_run)

    # Payload is the same for all POST requests except destination branch
    payload = make_post_payload(data)
    for branch in branches:
        payload['destination']['branch']['name'] = branch

        # Debugging
        if verbosity >= 1:
            log_object(payload, 'Last POST payload')

        if dry_run:
            return

        res = call('post',
                   POST_PR_URL.format(user=pr.user, repo=pr.repo),
                   json=payload,
                   verbosity=verbosity)
        link = res.json()['links']['html']['href']
        print()
        print(f'-> {branch}  {link}')


def call(method, url, verbosity=0, **kwargs):
    """Add Basic Auth for Bitbucket API before querying URL with the HTTP method/verb

    We are using Basic Auth from app passwords. See script options at the top.
    """
    if BITBUCKET_USERNAME and BITBUCKET_APP_PASSWORD:
        auth = HTTPBasicAuth(BITBUCKET_USERNAME, BITBUCKET_APP_PASSWORD)
        kwargs.setdefault('auth', auth)
    else:
        raise RuntimeError('Please provide BITBUCKET_USERNAME and BITBUCKET_APP_PASSWORD '
                           '(see readme)')

    res = getattr(requests, method)(url, **kwargs)
    if not res.ok:
        method = method.upper()
        err = f'Failed to {method} {url}: {res.status_code} {res.reason}'
        if verbosity >= 1:
            try:
                log_object(res.json(), f'Error response {res.status_code} {method}')
            except ValueError:
                pass

        raise RuntimeError(err)
    return res


def parse_url(url):
    """Support either slug format ({user}/{repo}/{id}) or link from GUI

    Raises UrlFormatError on parsing failure
    """

    def validation_error(reason):
        example = 'https://bitbucket.org/{user}/{repo}/pull-requests/{id}'
        return UrlFormatError({
            'format': f'bad url format, expected url in format {example}, got: {url}',
            'endpoint': f'expected endpoint to be /pull-requests (e.g. {example}, got: {url}'
        }[reason])

    match = re.match(r'([^/]+)/([^/]+)/(\d+)', url)
    if match:
        return PullRequest(*match.groups())

    parsed = urlparse(url)
    path = parsed.path.split('/')

    error = None
    try:
        _, user, repo, endpoint, pr, *_ = path
        assert endpoint == 'pull-requests'
        return PullRequest(user, repo, pr)

    except ValueError:
        raise validation_error('format')

    except AssertionError:
        raise validation_error('endpoint')


def query_pr(user, repo, pr, verbosity=0):
    """GET pull request data from Bitbucket API"""
    api = GET_PR_URL.format(**locals())
    res = call('get', api, verbosity=verbosity)
    result = res.json()
    if verbosity >= 1:
        log_object(result, 'Last GET response')
    return result


def make_post_payload(params):
    """Generates a payload for Bitbucket pull requests API

    Ref https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Bworkspace%7D/%7Brepo_slug%7D/pullrequests#post
    """
    p = JsonPathIterator(params)  # syntactic sugar for accessing nested values
    payload = {
        'title': p.title._,
        'description': p.description._,
        'source': {
            'commit': {
                'hash': p.source.commit.hash._
            },
            'repository': {
                'name': p.source.repository.name._,
                'full_name': p.source.repository.full_name._,
            },
            'branch': {
                'name': p.source.branch.name._,
            }
        },
        'destination': {
            'repository': {
                'name': p.destination.repository.name._,
                'full_name': p.destination.repository.full_name._,
            },
            'branch': {
                'name': None,  # To be supplied by caller
            }
        },
        'rendered': params['rendered'],
    }
    # Not using real jsonpath, so use comprehension instead of just querying Array<Object>
    reviewers = [{'uuid': user['uuid']} for user in params.get('reviewers', [])]
    if reviewers:
        # Only add the key if there are reviewers
        payload['reviewers'] = reviewers

    return payload


def main():
    global verbosity

    desc = 'Duplicates a pull request to branches in the same destination repository'
    parser = ArgumentParser(description=desc)

    parser.add_argument('pullrequest',
                        help='Either URL of the PR to duplicate, or a slug identifying it. '
                             'URL format: https://bitbucket.org/{user}/{repo}/pull-requests/{id} '
                             'Slug format: {user}/{repo}/{id}')

    parser.add_argument('branches',
                        nargs='+',
                        metavar='branch',
                        help='Branches to duplicate the PR to. These should exist in the '
                             'same destination repository')

    parser.add_argument('--user',
                        metavar='username:app_password',
                        help='Bitbucket username and app password, concatenated with ":"')

    parser.add_argument('-y', '--yes',
                        action='store_true',
                        help='Skip the confirmation message')

    parser.add_argument('-n', '--dry-run',
                        action='store_true',
                        help='Print the POST payload and exit, instead of sending it to '
                             'Bitbucket API.')

    parser.add_argument('-v', '--verbose',
                        action='count',
                        default=0,
                        help='Enable debug logging')

    args = parser.parse_args()

    if args.verbose >= 2:
        enable_debug_logging()
    
    if args.user:
        global BITBUCKET_USERNAME, BITBUCKET_APP_PASSWORD
        BITBUCKET_USERNAME, BITBUCKET_APP_PASSWORD = args.user.split(':')

    multipr(pr_url=args.pullrequest,
            branches=args.branches,
            dry_run=args.dry_run,
            skip_check=args.yes,
            verbosity=args.verbose)


if __name__ == '__main__':
    main()
