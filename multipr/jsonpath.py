class BadPathError(KeyError):
    pass

class JsonPathReader:
    """Quick and dirty jsonpath implementation; only supports dot (.) delim; no memoization

    obj = {
        'spam': [
            0, 1, {'999': 'eggs'}, 3
        ]
    }
    j = JsonPathReader(obj)
    j['$.spam.2:.0.999']  # -> 'eggs'
    """
    def __init__(self, obj):
        self.obj = obj

    def __contains__(self, key):
        if key in self.obj:
            return True

    def __getitem__(self, key):
        return self.read(self.obj, key)

    def get(self, path, default):
        try:
            return self.read(self.obj, path)
        except ValueError:
            return default

    @classmethod
    def read(cls, obj, jsonpath):
        parseint = cls._parseint
        ref = obj
        for step in str(jsonpath).split('.'):
            if not step:
                continue

            if step == '$':
                ref = obj
                continue

            if step in ref:
                ref = ref[step]
                continue

            num = parseint(step)
            try:
                if isinstance(num, int):
                    ref = ref[num]
                    continue

                if isinstance(num, tuple):
                    ref = ref[num[0]:num[1]]
                    continue
            except IndexError:
                BadPathError(f'bad jsonpath {jsonpath}, list index "{step}" out of range')

            raise BadPathError(f'bad jsonpath {jsonpath}, parse failure at "{step}"')

        return ref

    @staticmethod
    def _parseint(s):
        try:
            return int(s)
        except ValueError:
            pass
        if ':' in s:
            try:
                start, end = [int(x) if x else None for x in s.split(':')]
                return (start, end)
            except ValueError:
                pass
        return None


class JsonPathIterator(JsonPathReader):
    """Allows fluent dot-chained keys to access nested values. Chain must end with ._
    Numeric keys cannot be used in the dot-chain. See examples below.

    obj = {
        'spam': {
            'ham': {
                'eggs': 123
            },
            999: {
                'eggs': 456
            }
        }
    }
    j = JsonPathIterator(obj)
    j.spam             # -> <JsonPathIterator object ...>
    j.spam.999         # -> SyntaxError
    j.spam.ham.eggs._  # -> 123
    j.spam[999].eggs._ # -> 456
    """
    def __getattr__(self, key):
        value = self.obj

        if key == '_':
            return value

        return self[key]

    def __getitem__(self, key):
        cls = self.__class__
        return cls(super().__getitem__(key))
